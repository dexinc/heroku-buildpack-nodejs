set_git_ssh_key() {
    if [ "$GIT_SSH_KEY" != "" ]; then
        echo "Detected GIT_SSH_KEY in enviroment"
        # -- create .ssh directory if not there to hold our ssh config and key
        # for bitbucket.org
        mkdir -p $HOME/.ssh
        # -- create private key to access bitbucket.org with git+ssh from env variable
        echo $GIT_SSH_KEY | base64 --decode > $HOME/.ssh/bitbucket_ssh.id_rsa
        # -- create ssh_config file for bitbucket.org host
        echo "Host bitbucket.org" > $HOME/.ssh/config
        echo " ServerAliveInterval 15" >> $HOME/.ssh/config
        echo " StrictHostKeyChecking no" >> $HOME/.ssh/config
        echo " UserKnownHostsFile=/dev/null" >> $HOME/.ssh/config
        echo " IdentityFile ~/.ssh/bitbucket_ssh.id_rsa" >> $HOME/.ssh/config
    else
        echo "No GIT_SSH_KEY found in environment"
    fi
}

unset_git_ssh_key() {
    if [ "$GIT_SSH_KEY" != "" ]; then
        # Clear that sensitive key data from the environment
        echo "Removing GIT_SSH_KEY from environment"
        export GIT_SSH_KEY=0
        unset GIT_SSH_KEY
    fi
}
